#ifndef __DELAY_H
#define __DELAY_H

#define Delay_us  delay_us
#define Delay_ms  delay_ms
#define Delay_s   delay_s

void delay_us(uint32_t us);
void delay_ms(uint32_t ms);
void delay_s(uint32_t s);

#endif
