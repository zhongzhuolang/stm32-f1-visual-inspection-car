#ifndef __AD_H
#define __AD_H

void AD_Init(void);
float AD_GetValue(void);

#endif
